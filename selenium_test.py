from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys

browser = webdriver.Chrome()
browser.get('https://demoqa.com/automation-practice-form')
firstname_field = WebDriverWait(browser, 10).until(EC.element_to_be_clickable((By.ID, 'firstName'))).send_keys('test')
lastname_field = WebDriverWait(browser, 10).until(EC.element_to_be_clickable((By.ID, 'lastName'))).send_keys('test')
gender_checkbox = WebDriverWait(browser, 10).until(
    EC.element_to_be_clickable((By.XPATH, '//label[text()="Male"]'))).click()
phone_field = WebDriverWait(browser, 10).until(EC.element_to_be_clickable((By.ID, 'userNumber'))).send_keys(
    '8960738255')
submit_button = WebDriverWait(browser, 10).until(EC.element_to_be_clickable((By.ID, 'submit')))
submit_button.send_keys(Keys.ENTER)
browser.quit()
